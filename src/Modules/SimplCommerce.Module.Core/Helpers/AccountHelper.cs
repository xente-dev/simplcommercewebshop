﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimplCommerce.WebHost.Helpers
{
    public class AccountHelper
    {
        public string CleanPhoneNumber(string str)
        {
            return new string(str.Where(char.IsDigit).ToArray());
        }

    }
}