﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SimplCommerce.Infrastructure.Data;
using SimplCommerce.Module.Payments.Models;
using SimplCommerce.Module.PaymentXente.Models;
using SimplCommerce.Module.PaymentXente.ViewModels;

namespace SimplCommerce.Module.PaymentXente.Controllers
{
    [Authorize(Roles = "admin")]
    [Route("api/xente")]
    public class XenteApiController : Controller
    {
        private readonly IRepositoryWithTypedId<PaymentProvider, string> _paymentProviderRepository;

        public XenteApiController(IRepositoryWithTypedId<PaymentProvider, string> paymentProviderRepository)
        {
            _paymentProviderRepository = paymentProviderRepository;
        }
        
        [HttpGet("config")]
        public async Task<IActionResult> Config()
        {
            var xenteProvider = await _paymentProviderRepository.Query().FirstOrDefaultAsync(x => x.Id == PaymentProviderHelper.XenteProviderId);
            var model = JsonConvert.DeserializeObject<XenteConfigForm>(xenteProvider.AdditionalSettings);
            return Ok(model);
        }
        
        [HttpPut("config")]
        public async Task<IActionResult> Config([FromBody] XenteConfigForm model)
        {
            if (ModelState.IsValid)
            {
                var xenteProvider = await _paymentProviderRepository.Query().FirstOrDefaultAsync(x => x.Id == PaymentProviderHelper.XenteProviderId);
                xenteProvider.AdditionalSettings = JsonConvert.SerializeObject(model);
                await _paymentProviderRepository.SaveChangesAsync();
                return Accepted();
            }

            return BadRequest(ModelState);
        }

        //[AllowAnonymous]
        //[HttpPost]
        //[Route("Demo")]
        //public IActionResult IpnCallBack(string TransactionId, string SubscriptionId, string AccountId, string RequestId, decimal Amount, [FromBody] IpnRequestModel ipnRequest)
        //{
        //    //log 
        //    //LOGGER.Info($"IPN Call Back Recieved Request - {JsonConvert.SerializeObject(ipnRequest)}");
        //    return Ok(new IpnRequestModel
        //    {
        //        AccountId = "256787744279",
        //        CorrelationId = "",
        //        CreatedOn = DateTime.UtcNow,
        //        ReferenceId = Guid.NewGuid().ToString("N").ToUpper(),
        //        RequestId = RequestId,
        //        ResponseCode = 0,
        //        ResponseMessage = "IPN Call Back Recieved By Demo Ipn Callback end point",
        //        SubscriptionId = SubscriptionId,
        //        TransactionId = TransactionId
        //    });
        //}

    }
}
