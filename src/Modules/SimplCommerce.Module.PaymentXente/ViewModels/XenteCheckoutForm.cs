﻿namespace SimplCommerce.Module.PaymentXente.ViewModels
{
    public class XenteCheckoutForm
    {
        public string PublicKey { get; set; }

        public int Amount { get; set; }

        public string ISOCurrencyCode { get; set; }
    }
}
