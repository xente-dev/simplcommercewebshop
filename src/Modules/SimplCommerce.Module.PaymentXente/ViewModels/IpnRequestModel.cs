﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimplCommerce.Module.PaymentXente.ViewModels
{
    public class IpnRequestModel
    {
        public string AccountId { get; set; }

        public string SubscriptionId { get; set; }

        public string TransactionId { get; set; }

        public string BatchId { get; set; }

        public string RequestId { get; set; }

        public Dictionary<string, object> RequestReference { get; set; }

        public string StatusMessage { get; set; }

        public int StatusCode { get; set; }

        public string CorrelationId { get; set; }

        public string CreatedOn { get; set; }

        public decimal Amount { get; set; }

        public string CurrencyCode { get; set; }

    }
}
