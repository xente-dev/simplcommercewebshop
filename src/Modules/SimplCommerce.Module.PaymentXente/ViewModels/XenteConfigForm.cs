﻿using System.ComponentModel.DataAnnotations;

namespace SimplCommerce.Module.PaymentXente.ViewModels
{
    public class XenteConfigForm
    {
        [Required]
        public string PublicKey { get; set; }

        [Required]
        public string PrivateKey { get; set; }

        public decimal PaymentFee { get; set; }
    }
}
